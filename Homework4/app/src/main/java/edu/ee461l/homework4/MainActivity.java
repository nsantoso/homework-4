package edu.ee461l.homework4;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.util.JsonReader;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ShareActionProvider;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.DefaultedHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

public class MainActivity extends Activity implements OnMapReadyCallback {

    private static final String TAG = "MainActivity";
    private static final String KEY = "AIzaSyCJvBpTEYWtYokwnD7hVBKBWPzn81VKGvo";
    private static final String URI = "https://maps.googleapis.com/maps/api/geocode/json";
    private HttpClient client;
    private GoogleMap map;
    private MapFragment mapFrag;
    public static FragmentManager fragmentManager;
    public static LocationManager locationManager;
    private ShareActionProvider shareProvider;

    public static boolean ready = false;
    public static LatLng location = new LatLng(0, 0);
    public static String formattedAddress = "";
    public static Marker current;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        client = new DefaultHttpClient();
        fragmentManager = getFragmentManager();
        mapFrag = (MapFragment) fragmentManager.findFragmentById(R.id.mapFrag);
        mapFrag.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        Spinner s = (Spinner) menu.findItem(R.id.spinner).getActionView(); // find the spinner
        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this.getActionBar()
                .getThemedContext(), R.array.spinner_list, android.R.layout.simple_spinner_dropdown_item); //  create the adapter from a StringArray
        s.setAdapter(mSpinnerAdapter); // set the adapter

        AdapterView.OnItemSelectedListener myChangeListener = new AdapterView.OnItemSelectedListener() {
            protected Adapter initializedAdapter=null;
            @Override
            public void onItemSelected(AdapterView<?> parentView, View arg1,
                                       int arg2, long arg3) {
                if(initializedAdapter != parentView.getAdapter() ) {
                    initializedAdapter = parentView.getAdapter();
                    return;
                }

                switch (arg2) {
                    case 0:
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        break;
                    case 1:
                        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        break;
                    case 2:
                        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        break;
                    case 3:
                        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        };


        s.setOnItemSelectedListener(myChangeListener); // (optional) reference to a OnItemSelectedListener, that you can use to perform actions based on user selection

        MenuItem share = menu.findItem(R.id.menu_item_share);
        //shareProvider = (ShareActionProvider) share.getActionProvider();

        return true;
    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (shareProvider != null) {
            shareProvider.setShareIntent(shareIntent);
        }
    }

    public boolean share(MenuItem item) {
        Intent i = new Intent(Intent.ACTION_SEND);
        if (formattedAddress.equals("")) {
            makeToast("First enter an address");
            return false;
        }

        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Check out this location!");
        i.putExtra(Intent.EXTRA_TEXT, formattedAddress);
        try
        {
            this.startActivity(Intent.createChooser(i, "Share..."));
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void makeToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    // API KEY: AIzaSyCJvBpTEYWtYokwnD7hVBKBWPzn81VKGvo
    public void submitAddress(View view) {
        // get the field and clear it
        EditText addressField = (EditText)findViewById(R.id.addressText);
        String address = addressField.getText().toString();
        addressField.setText("");

        // validate address
        if (address == null || address.length() == 0) {
            makeToast("Please enter a address");
            return;
        }

        GeocodingTask task = new GeocodingTask();
        Object[] pass = {address, null, null};
        task.execute(pass);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        //map.get
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    setLocation(location, formattedAddress);
                    break;
                case 1:
                    makeToast("Invalid address");
                    break;
                default:
                    break;
            }
        }
    };

    public void setLocation(LatLng ll, String formAdd) {
        makeToast("Away we go!");
        if (map == null)
            makeToast("Please wait");
        else {
            map.moveCamera(CameraUpdateFactory.newLatLng(ll));
            map.moveCamera(CameraUpdateFactory.zoomTo((float) 15.0));
            if (current != null)
                current.remove();
            current = map.addMarker(new MarkerOptions().position(ll).title(formAdd));
            current.showInfoWindow();

            Log.v(TAG, "Location changed");
        }
    }

    public void myLocation(View item) {
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (lastKnownLocation != null) {
            LatLng ll = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            setLocation(ll, "There you are, buddy!");
            formattedAddress = ll.toString();
        }
        else {
            makeToast("Could not access GPS at this time");
        }
    }


    private class GeocodingTask extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... adds) {
            // String parsing
            String address = (String) adds[0];
            //GoogleMap map = (GoogleMap) adds[1];
            address = address.replaceAll("\\s+","+");
            Log.d(TAG, "Received request: " + address);

            // setup the request
            HttpParams params = new BasicHttpParams();
            //params.setParameter("key", KEY);
            //params.setParameter("address", address);
            HttpUriRequest req = new HttpGet(URI + "?key=" + KEY + "&address=" + address);
            //req.setParams(params);
            req.addHeader("accept", "application/json");
            Log.d(TAG, req.getRequestLine().toString());

            // Execute the request

            double lon;
            double lat;
            String formAdd;
            try {
                HttpResponse resp;
                BufferedReader in;
                JSONObject json;
                resp = client.execute(req);

                // check for bad response
                if (resp.getStatusLine().getStatusCode() != 200) {
                    Log.e(TAG, "Failed, error code : " + resp.getStatusLine().getStatusCode() + " "
                            + resp.getStatusLine().getReasonPhrase());
                }
                else {
                    Log.v(TAG, "OK");
                }


                in = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
                String tmp;
                StringBuilder jsonString = new StringBuilder();
                while ((tmp = in.readLine()) != null) {
                    jsonString.append(tmp);
                }

                json = new JSONObject(jsonString.toString());
                Log.d(TAG, json.toString());

                if (json.getJSONArray("results").length() == 0) {
                    handler.sendEmptyMessage(1);
                    return null;
                }

                JSONObject location = json.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                lat = location.getDouble("lat");
                lon = location.getDouble("lng");

                formAdd = json.getJSONArray("results").getJSONObject(0).getString("formatted_address");


            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

            LatLng ll = new LatLng(lat, lon);
           // notifyAll();

            synchronized (location) {
                location = ll;
                formattedAddress = formAdd;
            }
            Log.v(TAG, "Got the response data: " + lat + " " + lon);

            handler.sendEmptyMessage(0);
            ready = true;
            return null;
        }
    }

}
